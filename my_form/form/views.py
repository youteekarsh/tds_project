from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from .forms import Form_design
from .models import Create_form


def index(request):
    errors = []
    code = request.POST.get('code')
    if not code:
        errors.append("Enter the Access Code")
        return render(request, 'form/index.html',{'errors': errors})
    elif code != 'AAAA':
        errors.append("WRONG ACCESS CODE!")
        return render(request, 'form/index.html',{'errors': errors})
    else:
        return redirect('fill_form')

def fill_form(request):
    if request.method == "POST":
        form = Form_design(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('end')
    else:
        form = Form_design()
    return render(request, 'form/fill_form.html', {'form': form})


def end(request):
    return render(request, 'form/end.html')


def all_forms(request):
    list = Create_form.objects.all()
    return render(request, 'form/all_forms.html', {'list': list})


def details(request, AccNumber):
    customer = Create_form.objects.get(AccNumber=AccNumber)
    return render(request, 'form/details.html', {'customer': customer})
