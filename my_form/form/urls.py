from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name = 'index'),
    path('fill_form', views.fill_form, name = 'fill_form'),
    path('end', views.end, name = 'end'),
    path('all_forms', views.all_forms, name = 'all_forms'),
    path('<int:AccNumber>/details/', views.details, name='details'),
]
